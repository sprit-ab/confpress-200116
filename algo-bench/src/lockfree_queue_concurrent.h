// ============================================================================
//
//       Filename:  lockfree_queue_concurrent.h
//
//    Description:  The naive implementation of the 
//                  lock free queue
//
//        Version:  1.0
//        Created:  15.09.2019 13:36:51
//       Revision:  none
//       Compiler:  g++
//
//         Author:  Andrew Bogdanov (AB), m@n0k.ru
//   Organization:  
//
// ============================================================================

#pragma once

#include "queue_ops_status.h"

#include <atomic>
#include <stdexcept>

namespace lockfree {
template < class T >
  class ConcurrentQueue
  {
    public:
      using Type = T;
    public:
      ConcurrentQueue( size_t max_count)
        : max_elements_(max_count)
        , data_(new T[max_elements_])
        , closed_(false)
        , ipush_(0)
        , ipop_(0)
        , ipopmax_(0)
      {
        if (!std::atomic_is_lock_free(&ipush_)) {
          throw std::runtime_error("atomic_size_t is not lock free");
        }
        if (!std::atomic_is_lock_free(&closed_)) {
          throw std::runtime_error("atomic_bool is not lock free");
        }
      };
      ~ConcurrentQueue (){};
      ConcurrentQueue(ConcurrentQueue&&) = delete;
      ConcurrentQueue(const ConcurrentQueue&) = delete;
    public:
      QueueOpStatus push( T element ) noexcept {
        const auto rlx = std::memory_order_relaxed;
        const auto rls = std::memory_order_release;
        // const auto acq = std::memory_order_acquire;
        size_t ipush, ipop; 
        do {
          ipop = ipop_.load(rlx);
          ipush = ipush_.load(rlx);
            if ( closed_ )
              return QueueOpStatus::Closed;
            if ( index(ipush+1) == index(ipop) )
              return QueueOpStatus::Full;
        } while ( !ipush_.compare_exchange_weak(ipush,ipush+1, rls, rlx) );
        data_[index(ipush)] = element;
        while (!ipopmax_.compare_exchange_weak(ipush, ipush+1, rls, rlx)) {
          std::this_thread::yield();
        }
        return QueueOpStatus::Success;
      }
      /** Wait until some object will be put to the queue.
       * \warning if wait returns null pointer, it means shutdown happens
       * \return pointer to object of null if shutdown
       * */
      QueueOpStatus pop( Type& element) noexcept {
        size_t ipop, ipopmax;
        const auto rlx = std::memory_order_relaxed;
        const auto rls = std::memory_order_release;
        // const auto acq = std::memory_order_acquire;
        do {
            ipop = ipop_.load(rlx);
            ipopmax = ipopmax_.load(rlx);
            if ( closed_ )
              return QueueOpStatus::Closed;
            if (index(ipop) != index(ipopmax)) {
              element = data_[index(ipop)];
              if (ipop_.compare_exchange_weak(ipop, ipop+1, rls, rlx))
                return QueueOpStatus::Success;
            }
            std::this_thread::yield();
        } while (true);
        return QueueOpStatus::Empty;
      }

      size_t size( void ) const noexcept {
        return ipopmax_.load() - ipop_.load();
      }
      void close() noexcept {
        closed_.store(true);
      }
      QueueOpStatus wait_empty() const noexcept {
        while( size() > 0 && !closed_ )
          std::this_thread::yield();
        if (closed_)
          return QueueOpStatus::Closed;
        return QueueOpStatus::Empty;
      }
    private:
      inline size_t index(size_t seq) {
        return seq % max_elements_;
      }
      const size_t max_elements_; // constant thread-safe
      T* data_;
      std::atomic<bool> closed_;
      std::atomic<size_t> ipush_;
      std::atomic<size_t> ipop_;
      std::atomic<size_t> ipopmax_;
};

}		// -----  end of namespace lockfree  ----- 
