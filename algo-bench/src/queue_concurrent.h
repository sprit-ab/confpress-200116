// ============================================================================
//
//       Filename:  queue_concurrent.h
//
//    Description:  The safe thread queue
//
//        Version:  1.0
//        Created:  14.09.2019 17:58:02
//       Revision:  none
//       Compiler:  g++
//
//         Author:  Andrew Bogdanov (AB), m@n0k.ru
//   Organization:  
//
// ============================================================================
#pragma once

#include "queue_ops_status.h"

#include <memory>
#include <queue>
#include <mutex>
#include <condition_variable> 
#include <atomic>

namespace locking {

/*** \class ConcurrentQueue
 *   \brief  The thread safe queue.
 *   This queue is limited by max_elements_
 *   It based on std::queue with mutex guard
 */
template < class T >
  class ConcurrentQueue
  {
    public:
      using Type = T;
    public:
      ConcurrentQueue( size_t max_count)
        : closed_(false)
        , max_elements_(max_count)
      {};
      ~ConcurrentQueue (){};
      ConcurrentQueue(ConcurrentQueue&&) = delete;
      ConcurrentQueue(const ConcurrentQueue&) = delete;
    public:
      QueueOpStatus push( T element ) {
        std::lock_guard< std::mutex >  lock( mutex_ );
        if (queue_.size() >= max_elements_)
          return QueueOpStatus::Full;
        if ( closed_ )
          return QueueOpStatus::Closed;
        queue_.push( element );
        changed_.notify_one();
        return QueueOpStatus::Success;
      }
      /** Wait until some object will be put to the queue.
       * \warning if wait returns null pointer, it means shutdown happens
       * \return poiter to object of null if shutdown
       * */
      QueueOpStatus pop( Type& element) {
        std::unique_lock<std::mutex> lock(mutex_);

        while( queue_.empty() && !closed_ )
          changed_.wait(lock);

        if ( closed_ )
          return QueueOpStatus::Closed;

        element = queue_.front();
        queue_.pop();
        changed_.notify_all();
        lock.unlock();
        return QueueOpStatus::Success;
      }
      size_t size( void ) const {
        std::lock_guard<std::mutex> lock(mutex_);
        return queue_.size();
      }
      void close() noexcept {
        std::unique_lock<std::mutex> lock(mutex_);
        closed_ = true;
        changed_.notify_all();
      }
      QueueOpStatus wait_empty() {
        std::unique_lock<std::mutex> lock(mutex_);
        while( !queue_.empty() && !closed_ )
          changed_.wait(lock);
        if (closed_)
          return QueueOpStatus::Closed;
        return QueueOpStatus::Empty;
      }
    private:
      std::queue<Type> queue_;
      bool closed_;
      size_t max_elements_;
      mutable std::mutex mutex_;
      std::condition_variable  changed_;
  }; // ----------  end of template class ConcurrentQueue  ---------- 
}		// -----  end of namespace locking  ----- 
