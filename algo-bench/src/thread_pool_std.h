// ============================================================================
//
//       Filename:  thread_pool_std.h
//
//    Description: Thread pool based on std threads
//
//        Version:  1.0
//        Created:  15.09.2019 08:44:09
//       Revision:  none
//       Compiler:  g++
//
//         Author:  Andrew Bogdanov (AB), m@n0k.ru
//   Organization:  
//
// ============================================================================

#pragma once 

#include <vector>
#include <thread>
#include "queue_ops_status.h"

/**
 * \class ThreadPool
 * \brief thread pool template realization
 *
 * template parameters:
 *
 * T_ - the type element
 * Fn_ - the functor, which will be processed the element
 *      Example:
 * Queue_ - the ConcurrentQueue 
 */
template< typename T_,
          typename Fn_,
          typename Queue_ >
class ThreadPool : public Queue_
{
  public:
    using Queue = Queue_;
    using Type = T_;
    using Fn = Fn_;
  public:
    template <typename ...QueueArgs> 
    ThreadPool( size_t threads_count, QueueArgs... queue_args)
      : Queue_(queue_args...)
      , threads_(threads_count)
    {
    };
    ~ThreadPool(){
      deinit();
    }
  public:
    template< typename OtherFn >
    bool init(OtherFn fn)
    {
      fn_ = std::move(fn);
      for ( auto& t : threads_ )
        t = std::move(std::thread(&ThreadPool<Type,Fn,Queue>::main, this));
      return true; 
    };
    void deinit() 
    {
      Queue::close();
      for (auto& t : threads_ )
        if ( t.joinable() )
          t.join();
    };
  protected:
    std::vector<std::thread> threads_;
    Fn fn_;
    void main()
    {
      Type el;
      while ( true  )
        switch ( Queue::pop(el) ) {
          case QueueOpStatus::Success: 
            fn_(el);
          break;
          case QueueOpStatus::Closed: 
          return; // exit
          case QueueOpStatus::Empty: 
          case QueueOpStatus::Full: 
            // this is strage
          break;
        }
    }
  private:
}; /* -----  end of class ThreadPool  ----- */

