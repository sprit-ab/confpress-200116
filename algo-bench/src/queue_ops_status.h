// ============================================================================
//
//       Filename:  queue_ops_status.h
//
//    Description:  The simple enum which share bettween implementation
//
//        Version:  1.0
//        Created:  15.09.2019 13:38:12
//       Revision:  none
//       Compiler:  g++
//
//         Author:  Andrew Bogdanov (AB), m@n0k.ru
//   Organization:  
//
// ============================================================================
#pragma once

enum class QueueOpStatus {
  Success,
  Empty,
  Full,
  Closed
};

