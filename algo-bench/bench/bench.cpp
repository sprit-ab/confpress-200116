// ============================================================================
//
//       Filename:  bench.cpp
//
//    Description:  
//
//        Version:  1.0
//        Created:  05.01.2020 23:42:13
//       Revision:  none
//       Compiler:  g++
//
//         Author:  Andrew Bogdanov (AB), m@n0k.ru
//   Organization:  
//
// ============================================================================

#include "thread_pool_std.h"
#include "lockfree_queue_concurrent.h"
#include "queue_concurrent.h"
#include <benchmark/benchmark.h>
#include <functional>
#include <stdexcept>

template <typename Queue>
class SpeedTestQueue
{
  public:
    SpeedTestQueue()
      : tp(kThreads,kMaxElements * 2) {
        auto fn = [this](int i){
          if ( i < kMaxValue ) {
            tp.push(i+1);
          }
        };
        if ( !tp.init(std::move(fn)) )
          throw std::runtime_error("Could not initialize the thread pool");
    }

    void exec() {
      for (int i = 0; (size_t)i < kMaxElements; ++i) {
        if ( QueueOpStatus::Success != tp.push(i) )
          throw std::runtime_error("Could not push an element");
      }
      if ( QueueOpStatus::Empty != tp.wait_empty() )
        throw std::runtime_error("Could not wait until queue is empty");
    }

    ~SpeedTestQueue(){
      tp.deinit();
    }

  private:
    const int kMaxValue = 10000;
    const size_t kMaxElements = 100;
    const size_t kThreads = 4;
    ThreadPool<int,std::function<void (int)>,Queue> tp;
};

static void BM_LockConcurentQueue(benchmark::State& state) {
  SpeedTestQueue<locking::ConcurrentQueue<int>> test;
  for (auto _ : state) {
    test.exec();
  }
}

static void BM_LockFreeConcurentQueue(benchmark::State& state) {
  SpeedTestQueue<lockfree::ConcurrentQueue<int>> test;
  for (auto _ : state) {
    test.exec();
  }
}

// Register the function as a benchmark
BENCHMARK(BM_LockConcurentQueue);
BENCHMARK(BM_LockFreeConcurentQueue);

BENCHMARK_MAIN();
