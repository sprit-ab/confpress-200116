// ============================================================================
//
//       Filename:  main.cpp
//
//    Description:  
//
//        Version:  1.0
//        Created:  08.01.2020 23:31:22
//       Revision:  none
//       Compiler:  g++
//
//         Author:  Andrew Bogdanov (AB), m@n0k.ru
//   Organization:  
//
// ============================================================================

#include <iostream>
#include	<stdlib.h>

#include "thread_pool_std.h"
#include "lockfree_queue_concurrent.h"
#include "queue_concurrent.h"
#include <string.h>
#include <functional>

template <typename Queue>
bool SpeedTestQueue()
{
  const int kMaxValue = 10000;
  const size_t kMaxElements = 100;
  const size_t kThreads = 4;
  ThreadPool<int,std::function<void (int)>,Queue> tp(kThreads,kMaxElements * 2);
  auto fn = [&tp](int i){
    if ( i < kMaxValue ) {
      tp.push(i+1);
    }
  };
  if ( !tp.init(std::move(fn)) )
    return EXIT_FAILURE;
  for (int i = 0; (size_t)i < kMaxElements; ++i) {
    if ( QueueOpStatus::Success != tp.push(i) )
      return EXIT_FAILURE;
  }
  if ( QueueOpStatus::Empty != tp.wait_empty() )
    return EXIT_FAILURE;
  tp.deinit();
  return EXIT_SUCCESS;
}

int main ( int argc, char *argv[] ) {
  using namespace std;
  if ( argc == 2 ) {
    const char *fnname = argv[1];
    if ( strncmp(fnname,"l",2) == 0 )
      return SpeedTestQueue<locking::ConcurrentQueue<int>>();
    else if ( strncmp(fnname,"lf",3) == 0 )
      return SpeedTestQueue<lockfree::ConcurrentQueue<int>>();
  }
  cerr << "Please use:" << endl
       << " consol <lf|l>" << endl;
  return EXIT_FAILURE;
}				// ----------  end of function main  ----------
